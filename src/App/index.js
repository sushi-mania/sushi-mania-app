import { BrowserRouter as Router, Route } from 'react-router-dom';
import { cartReducers } from '@sushi-mania/cart';
import { createStore, combineReducers } from 'redux';
import { ProductList, productReducers } from '@sushi-mania/product-list';
import { Provider } from 'react-redux';
import Header from '@sushi-mania/header';
import React, { Fragment } from 'react';

import Cart from '../Cart';

const store = createStore(
  combineReducers({ products: productReducers, cart: cartReducers }),
  process.env.NODE_ENV !== 'production' && window.__REDUX_DEVTOOLS_EXTENSION__
    ? window.__REDUX_DEVTOOLS_EXTENSION__()
    : undefined,
);

export default function App() {
  return (
    <Provider store={store}>
      <Router>
        <Fragment>
          <Header />
          <Route exact path="/" component={ProductList} />
          <Route exact path="/my-cart" component={Cart} />
        </Fragment>
      </Router>
    </Provider>
  );
}
