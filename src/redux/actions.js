export const EMPTY_Cart = 'EMPTY_Cart';
export const emptyCart = () => ({
  type: EMPTY_Cart,
});

export const REMOVE_PRODUCT = 'REMOVE_PRODUCT';
export const removeProduct = productReference => ({
  type: REMOVE_PRODUCT,
  payload: {
    productReference,
  },
});

export const UPDATE_PRODUCT_QUANTITY = 'UPDATE_PRODUCT_QUANTITY';
export const updateProductQuantity = (productReference, deltaQuantity) => ({
  type: UPDATE_PRODUCT_QUANTITY,
  payload: {
    deltaQuantity,
    productReference,
  },
});
