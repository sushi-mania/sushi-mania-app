import { EMPTY_Cart, REMOVE_PRODUCT, UPDATE_PRODUCT_QUANTITY } from './actions';

const initialState = {
  products: [],
};

const resetQuantities = prevState => ({
  ...prevState,
  products: prevState.products.map(({ quantity, ...otherPros }) => ({
    ...otherPros,
    quantity: 0,
  })),
});

const resetQuantity = (prevState, productReference) => ({
  ...prevState,
  products: prevState.products.map(({ reference, quantity, ...otherPros }) => {
    const updatedQuantity = productReference === reference ? 0 : quantity;

    return {
      ...otherPros,
      reference,
      quantity: updatedQuantity,
    };
  }),
});

const updateQuantity = (prevState, productReference, deltaQuantity) => ({
  ...prevState,
  products: prevState.products.map(({ reference, quantity, ...otherPros }) => {
    const updatedQuantity = productReference === reference ? Math.max(0, quantity + deltaQuantity) : quantity;

    return {
      ...otherPros,
      reference,
      quantity: updatedQuantity,
    };
  }),
});

export default function reducers(prevState = initialState, { payload, type }) {
  switch (type) {
    case EMPTY_Cart:
      return resetQuantities(prevState);
    case REMOVE_PRODUCT:
      return resetQuantity(prevState, payload.productReference);
    case UPDATE_PRODUCT_QUANTITY:
      return updateQuantity(prevState, payload.productReference, payload.deltaQuantity);
    default:
      return prevState;
  }
}
