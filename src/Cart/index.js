import { connect } from 'react-redux';

import { emptyCart, removeProduct, updateProductQuantity } from '../redux/actions';
import Cart from './component';

const mapStateToProps = state => ({
  items: state.products.filter(({ quantity }) => quantity > 0),
});

const mapDispatchToProps = {
  emptyCart,
  removeProduct,
  updateProductQuantity,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Cart);
