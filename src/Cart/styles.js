import styled from 'styled-components';

export const StyledCart = styled('div')({
  color: 'black',
  padding: '1em',
});

export const StyledCartItems = styled('ol')({
  display: 'flex',
  flexDirection: 'column',
  margin: 0,
});
