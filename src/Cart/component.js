import { arrayOf, func, shape } from 'prop-types';
import { Button } from '@sushi-mania/ui';
import { CartItem, cartItemTypes } from '@sushi-mania/cart';
import { Redirect } from 'react-router-dom';
import React, { Component, Fragment } from 'react';

import { StyledCart, StyledCartItems } from './styles';

class Cart extends Component {
  constructor() {
    super();

    this.state = {
      showConfirmMessage: false,
    };
  }

  handleRemoveAllButtonClick = () => {
    const { emptyCart } = this.props;

    emptyCart();
  };

  handleConfirmButtonClick = () => {
    const { emptyCart } = this.props;

    this.showConfirmMessage();
    emptyCart();
    setTimeout(this.hideConfirmMessage, 3000);
  };

  showConfirmMessage = () => {
    this.setState(() => ({
      showConfirmMessage: true,
    }));
  };

  hideConfirmMessage = () => {
    this.setState(() => ({
      showConfirmMessage: false,
    }));
  };

  renderItems() {
    const { items, removeProduct, updateProductQuantity } = this.props;

    return items.map(({ reference, ...otherProps }) => (
      <CartItem
        key={reference}
        reference={reference}
        removeProduct={removeProduct}
        updateProductQuantity={updateProductQuantity}
        {...otherProps}
      />
    ));
  }

  renderConfirmMessage() {
    const { showConfirmMessage } = this.state;

    if (!showConfirmMessage) return null;

    return <h3>Votre commande a bien été prise en compte</h3>;
  }

  renderContent() {
    const { items } = this.props;

    if (items.length === 0) return null;

    const totalCount = items.reduce((sum, { quantity }) => sum + quantity, 0);
    const totalPrice = items.reduce((sum, { price, quantity }) => sum + quantity * price, 0);

    return (
      <Fragment>
        <h2>{`${totalPrice.toFixed(2)} € - ${totalCount} produits(s)`}</h2>
        <Button action={this.handleRemoveAllButtonClick} size="small">
          Vider mon panier
        </Button>
        <StyledCartItems>{this.renderItems()}</StyledCartItems>
        <Button action={this.handleConfirmButtonClick} size="big">
          Valider ma commande
        </Button>
      </Fragment>
    );
  }

  render() {
    const { items } = this.props;
    const { showConfirmMessage } = this.state;

    if (items.length === 0 && !showConfirmMessage) return <Redirect to="/" />;

    return (
      <StyledCart>
        {this.renderContent()}
        {this.renderConfirmMessage()}
      </StyledCart>
    );
  }
}

Cart.defaultProps = {
  emptyCart: Function.prototype,
  removeProduct: Function.prototype,
  updateProductQuantity: Function.prototype,
  items: [],
};

Cart.propTypes = {
  emptyCart: func,
  removeProduct: func,
  updateProductQuantity: func,
  items: arrayOf(shape(cartItemTypes)),
};

export default Cart;
