# 🍣 Sushi Cdiscount

_Sushi e-shop web app built with [React](https://github.com/facebook/react)._

**This reposotitory was built to show a folder structure that works at Cdiscount.**

## Getting Started

Install all dependencies:

```
yarn
```

Run development server:

```
yarn start
```

## Demo

You can test the [demo version](https://dazzling-hoover-69eb6a.netlify.com/) online.

It uses a mocked REST API built with [json-server](https://github.com/typicode/json-server) and [deployed](https://github.com/jesperorb/json-server-heroku#deploy-to-heroku) with [Heroku](https://dashboard.heroku.com) at [https://sdiscount-api.herokuapp.com/](https://sdiscount-api.herokuapp.com/).
